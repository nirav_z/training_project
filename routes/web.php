<?php

use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 Auth::routes();
Route::get('/', function () {
    return redirect('login');
});
Route::get('test_form', function () {
    return view('test_form');
});
Route::post("test_post_form",'Dashbordcontroller@test_form')->name("test_form"); 


// Route::view("login", "auth/login")->name('login')->withoutMiddleware(['auth']);
// Route::post("login", 'Logincontroller')->name('login')->withoutMiddleware(['auth']);
 Route::group(['middleware'=>'web'],function(){
    Route::get("home",'Dashbordcontroller@show')->name("dashbord"); 
    //login Route
        Route::post("logins",'Dashbordcontroller@newView')->name("logins"); 
    Route::get("users",'Dashbordcontroller@getData');
    //  Auth::routes();
    // Route::get('/home', 'HomeController@index')->name('home');
    Route::get("product/add",'ProductController@add')->name("product-add"); 
    Route::post("save_product",'ProductController@addData')->name("save_product");
    Route::get("product",'ProductController@index')->name("product");
    Route::get("product/edit/{id}",'ProductController@showData')->name("product-edit");
    Route::post("product/edit",'ProductController@update')->name("product-edit");
    Route::post('product/delete','ProductController@delete')->name("product_delete");

 }); 