<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNewptoductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->string('product_sku')->nullable()->change(); 
            $table->string('product_category')->nullable()->change();     
            $table->string('product_Brand')->nullable()->change();     
            $table->string('product_color')->nullable()->change();     
            $table->string('product_dimension')->nullable()->change();     
            $table->string('product_price')->nullable()->change();     
            $table->string('product_discount')->nullable()->change();     
            $table->string('product_warranty')->nullable()->change();     
            $table->string('product_warranty_for')->nullable()->change();     
            $table->string('warranty_description')->nullable()->change();     
            $table->string('product_description')->nullable()->change(); 
            $table->string('product_Quantity')->nullable()->change();     
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
