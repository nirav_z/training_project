<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->string('product_sku');
            $table->string('product_Brand');
            $table->string('product_color');
            $table->string('product_dimension');
            $table->integer('product_price');
            $table->integer('product_discount');
            $table->integer('product_Quantity');
            $table->tinyInteger('product_warranty');
            $table->string('product_warranty_for');
            $table->text('warranty_description');
            $table->text('product_description');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
