<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    public function rules()
    {
        return [
            'product_color' => 'required'
        ];
    }
    //
    // use HasFactory;
    public function imgs()
    {
        return $this->hasOne('App\image','product_id', 'id');

    }
    
}
