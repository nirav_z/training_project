<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\product;
use App\image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Validator;


class ProductController extends Controller
{
    //
    public function addData(Request $req)
    {
      
        //  dd($req->all());
            $req->validate([
           'product_name'=>'required',
           'product_sku'=>'required',
           'product_Brand'=>'required',
           'product_price'=>'required',
           'product_images' => 'required|mimes:jpeg,png'

         ]);
        // dd($validated);
           $product= new product; 
           $product->product_name=$req->product_name;
           $product->product_sku=$req->product_sku;
           $product->product_category=$req->product_category;
           $product->product_Brand=$req->product_Brand;
           $product->product_color=$req->product_color;
           $product->product_dimension=$req->product_dimension;
           $product->product_price=$req->product_price;
           $product->product_discount=$req->product_discount;
           $product->product_Quantity=$req->product_Quantity;
           $product->product_warranty=$req->product_warranty;
           $product->product_warranty_for=$req->product_warranty_for;
           $product->warranty_description=$req->warranty_description;
           $product->product_description=$req->product_description;
           $file= Storage::disk('public')->putFile('doc', new File($req->file('product_images')));
        
        //    $imageName = time() . '.' . $req->file('product_images')->getClientOriginalExtension();
        //    $destinationPath = '../storage/app/public/doc';
        //    $upload_success = $req->file('product_images')->move($destinationPath, $imageName);
        //    $req->file('product_images')->store('doc');
        //    $filename = time() . '.' . $image->getClientOriginalExtension();  
        //    $product = $product->create()
        $product->save();

       $image=new image;

        $image->image_path=$file;
        $image->product_id=$product->id;
        $image->save();
           
           
           return redirect('product');
           // return student::all();
    }
    function index()
    {
        $data = new product;
         $data = $data->with('imgs')->orderBy('id','DESC')->get();
        //  dd($data);
         return view('products.index',['product'=>$data]);
            
    }
    function add()
    {
        return view('products.add');

    }
    function showData($id)
    {
        
      $data = product::find($id);

      return view('products.edit',['data'=>$data]);

    }
    public function update(Request $req)
    {
        $req->validate([
            'product_name'=>'required',
            'product_sku'=>'required',
            'product_Brand'=>'required',
            'product_price'=>'required',
            'product_images' => 'required|mimes:jpeg,png'

 
          ]);
 
           $product=product::find($req->id);
           $product->product_name=$req->product_name; 
           $product->product_sku=$req->product_sku;
           $product->product_category=$req->product_category;
           $product->product_Brand=$req->product_Brand;
           $product->product_color=$req->product_color;
           $product->product_dimension=$req->product_dimension;
           $product->product_price=$req->product_price;
           $product->product_discount=$req->product_discount;
           $product->product_Quantity=$req->product_Quantity;
           $product->product_warranty=$req->product_warranty;
           $product->product_warranty_for=$req->product_warranty_for;
           $product->warranty_description=$req->warranty_description;
           $product->product_description=$req->product_description;
           
           $product->save();

           
           return redirect('product');
           // return student::all();
    }
    public function delete(Request $req)
{   
    // dd($req->all());
    //For Deleting Users
    // $product = new product;
    $product = product::find($req->id);
    $product->delete();
    return response()->json(['success'=>'Record has beendeleted']);
         
}
    // function save()
    // {
    //     dd($request->all());
    // }

    
}
