
@extends('layout')
  @section('content')
  
<div class="card card-rounded">
    <div class="card-body p-0">
      <div class="row">
        @csrf
                      <div class="col-lg-12 d-flex flex-column">                        
                         <div class="row flex-grow">
                          <div class="col-12 grid-margin stretch-card">
                            <div class="card card-rounded">
                              <div class="card-body">
                                <div class="d-sm-flex justify-content-between align-items-start">
                                  <div>
                                    <h4 class="card-title card-title-dash">Product List</h4>
                                   <p class="card-subtitle card-subtitle-dash">You have 50+ new List</p>                           
                                  </div>
                                  
                                  <div>
                                    <a href="{{Route('product-add')}}" class="btn btn-primary btn-lg text-white mb-0 me-0"><i class="mdi mdi-account-plus"></i>Add Product</a>
               
                                  </div>
                                </div>
                                <div class="table-responsive  mt-1">
                                  <table class="table select-table">
                                    <thead>
                                      <tr>
                                        <th scope="col">Sr. No.</th>
                                        <th scope="col">Product Name</th>
                                        <th scope="col">Product SKU</th>
                                        <th scope="col">Category</th>
                                        <th scope="col">Brand</th>
                                        <th scope="col">Color</th>
                                        <th scope="col">Dimension</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Discount</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Warranty Available</th>
                                        <th scope="col">Warranty For</th>
                                        <th scope="col">Warranty Description</th>
                                        <th scope="col">Product Description</th>
                                        <th scope="col"> Image</th>
                                        <!-- <th scope="col"> Images Name</th> -->
                                        <th style="width: 100px">Action</th>


                                      </tr>
                                    </thead>
                                    <tbody>
                                           @foreach($product as $i => $product)

                                                <tr id="product-{{$product['id']}}">
                                                        <!-- <th scope="row">1</th> -->
                                                        <td>{{$i+1}}</td>
                                                        <td>{{$product['product_name']}}</td>
                                                        <td>{{$product['product_sku']}}</td>
                                                        <td>{{$product['product_category']}}</td>
                                                        <td>{{$product['product_Brand']}}</td>
                                                        <td>{{$product['product_color']}}</td>
                                                        <td>{{$product['product_dimension']}}</td>
                                                        <td>{{$product['product_price']}}</td>
                                                        <td>{{$product['product_discount']}}</td>
                                                        <td>{{$product['product_Quantity']}}</td>
                                                        <td>{{$product['product_warranty']}}</td>
                                                        <td>{{$product['product_warranty_for']}}</td>
                                                        <td>{{$product['warranty_description']}}</td>
                                                        <td>{{$product['product_description']}}</td>
                                                        <td><img src="{{ asset('/storage/'.$product['imgs']['image_path']) }}" alt="" title=""></a></td>
                                                         <!-- <td>{{$product['imgs']['image_path']}}</td> -->
                                                        <td style="display: flex;"><a style="font-size: 12px" href ="{{'product/edit/'.$product['id']}}"  name="Details" class="btn btn-primary">Edit</a>
                                                         <a style="font-size: 12px; margin-left: 2px" href="javascript:void(0)"  name="delete" class="btn btn-danger btn-delete" data-id="{{$product['id']}}">Delete</a></td>
                                                       
                                                 </tr>
                                              @endforeach    
                                  </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
      // function delete(id)
      // {
      //   if(confirm("Do You realy want to delete this record?"))
      //   {
      //     $.ajax({
      //       url:'/product/delete/'+id,
      //       type:'DELETE',
      //       data:{
      //           _tokan : $("input[name=_tokan]").val()
      //       },
      //       success:function(response)
      //       {
      //         $(id).remove();
      //       }
      //     });
      //   }
      // }
      $(".btn-delete").click(function(){ 
        // console.log("hi");
         var id = $(this).data("id");
        if(confirm("Do You realy want to delete this record?"))
        {
          $.ajax({
            url:"{{ route ('product_delete') }}",
            type:'post',
            data:{
                _token : "{{csrf_token()}}",
                id:id
            },
            success:function(response)
            {
              $('#product-'+id).remove();
            }
          });
        }
      });
</script>



 @endsection