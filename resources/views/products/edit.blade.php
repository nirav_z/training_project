
@extends('layout')
  @section('content')
   

<div class="card col-md-12">
<div class="card-body">
<form class="container" id="form" action="{{route('product-edit',['id' => $data['id']])}}" method="post" enctype='multipart/form-data'>
    @csrf
    <center><h1><b><u>Edit </u> <u> Product</u></b></h1></center><br>
    <div class="form-group">
    <label for="inputAddress">Product Name</label>
    <input type="hidden" class="form-control" id="" name="id"  value="{{$data['id']}}">
    <input type="text" class="form-control" id="product_name"  name="product_name" value="{{$data['product_name']}}" placeholder="Enter product Name">
    @error('product_name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="inputAddress">Product SKU</label>
    <input type="text" class="form-control" id="product_sku"  name="product_sku" value="{{$data['product_sku']}}" placeholder="Enter pruduct SKU">
    @error('product_sku')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  
<div class="form-group">
<label for="inputState">Category</label>
      <select id="inputState" class="form-control" name="product_category">
      @php    
          $cat = ["Electronics","Clothing","Footwear","Watches","Books"];
      @endphp
      @foreach($cat as $value)
      <option  value="{{$value}}" 
          {{ ($data['product_category'] == $value) ? 'selected' : '' }}
      >
          {{$value}}
      </option>
      @endforeach
      </select>
</div>
  <div class="form-group">
    <label for="inputAddress">Brand</label>
    <input type="text" class="form-control" id="product_Brand" name="product_Brand" value="{{$data['product_Brand'] ?? null}}" placeholder="Enter The Brand">
    @error('product_Brand')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="inputAddress">Color</label>
    <input type="text" class="form-control" id="" name="product_color" value="{{$data['product_color']}}" placeholder="Enter  Color">
  </div>
  <div class="form-group">
    <label for="inputAddress">Dimension</label>
    <input type="text" class="form-control" id="" name="product_dimension" value="{{$data['product_dimension']}}" placeholder="Enter Dimension">
  </div>
  <div class="form-group">
    <label for="inputAddress">Price</label>
    <input type="text" class="form-control" id="product_price" name="product_price" value="{{$data['product_price']}}" placeholder="Enter Price">
    @error('product_price')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="inputAddress">Discount</label>
    <input type="text" class="form-control" id="" name="product_discount" value="{{$data['product_discount']}}" placeholder="Enter Discount">
  </div>
 
  <div class="form-group">
<label for="inputState">Quantity</label>
<select id="year" name="product_Quantity" class="form-control ">
    @for ($i = 1; $i <=100; $i++)
        <option {{ ($data['product_Quantity'] == $i) ? 'selected' : '' }}
value="{{ $i }}">{{ $i }}</option>
    @endfor
</select>
</div>
<div class="form-groups">
  <div class="form-group">
  <label for="inputZip">Warranty Available </label><br>
            <input class="" type="radio" name="product_warranty" id="show" value="1" {{ ($data['product_warranty'] == '1') ? 'checked' : '' }}>

            <label class="" for="">
                Yes
            </label>&nbsp;&nbsp;
                <input class="" type="radio" name="product_warranty" id="hide" value="1" {{ ($data['product_warranty'] == '0') ? 'checked' : '' }}
>
            <label class="" for="">
                No
            </label>   
</div>
<div class="form-group remove_warranty">
<label for="inputState">Warranty For</label>
      <select id="inputState" class="form-control" name="product_warranty_for">
      @php    
          $war = ["3 Months","6 Months","1 Year","2 Years","5 Years"];
      @endphp
      @foreach($war as $value)
      <option  value="{{$value}}" 
          {{ ($data['product_warranty_for'] == $value) ? 'selected' : '' }}
      >
          {{$value}}
      </option>
      @endforeach
      </select>
</div>
  <div class="form-group remove_warranty">
    <label for="inputAddress2">Warranty Description</label><br>
    <!-- <textarea id="" name="warranty_description" value="{{$data['warranty_description']}}" rows="4" cols="100" placeholder="Type A Product Description"> </textarea> -->
    <textarea id="inputDescriptionEs" class="form-control" name="warranty_description" rows="4" >@isset($data){{$data->warranty_description}}@else @endIf</textarea>

  </div>
  </div>
   <div class="form-group">
    <label for="inputAddress2">Product Description</label><br>
    <!-- <textarea id="" name="product_description" value="{{$data['product_description']}}" rows="4" cols="100" placeholder="Type A Product Description"> </textarea> -->
    <textarea id="inputDescriptionEs" class="form-control" name="product_description" rows="4" >@isset($data){{$data->product_description}}@else @endIf</textarea>

  </div>
  <div class="form-group">
    <label for="inputAddress2">Product Images</label>
    <input type="file" class="form-control" id="inputAddress" name="product_images" value="" placeholder="">
    <img src="{{ asset('/storage/'.$data['imgs']['image_path']) }}" with="70pc" height="70px" alt="img">
    @error('product_images')
      <div class="error" style="font-size: 12px">{{ $message }}</div>
    @enderror
  </div>
  <input type="submit" class="btn btn-primary" id="submitbtn" value="Submit">
</form>
</div>
</div>

<!-- Click event for Warranty Available-->
<script >
  $(document).ready(function(){
    $("#hide").click(function(){
        $(".remove_warranty").hide();
    });
    $("#show").click(function(){
      $(".remove_warranty").show();
    });
 });
 $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {0}');
 $("#form").validate({
      rules: {
          product_name: 'required',
          product_sku: 'required',
          product_Brand: 'required',
          product_price: {
            required: true,
            number: true
          } ,  
          product_images: {
            required:true,
            accept:"jpg,jpeg,png",
            filesize: 1000000   //max size 1 mb
          },
      },
      messages: {
        product_name: 'This field is required',
        product_sku: 'This field is required',
        product_Brand: 'This field is required',
        product_price: 'This field is required',
        product_images: {filesize:" file size must be less than 1 MB.",
                    accept:"Please upload jpg, png, jpeg file.",
                    required:"This field is required."},
      },
      submitHandler: function(form) {
          form.submit();
      }
  });
   

</script>


 @endsection